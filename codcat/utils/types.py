"""Common types for codcat project."""

import pathlib
import typing as t

PathLike = t.Union[pathlib.Path, str]
